//@ts-check

import { getConfig } from "./utils/getConfig.js";
import commands from "./commands/_.js";

export const bot = () => {
  const { Client, Intents } = require("discord.js");
  const { TOKEN, REGISTERED } = getConfig();

  if (!REGISTERED) {
    throw new Error(
      `you need to register commands with the register command first`
    );
  }

  const client = new Client({ intents: [Intents.FLAGS.GUILDS] });

  client.once("ready", () => {
    console.log("Ready!");
  });

  client.on("interactionCreate", async (interaction) => {
    if (!interaction.isCommand()) {
      return;
    }

    const { commandName } = interaction;

    if (commandName in commands) {
      const action = commands[commandName];
      await action.reply(interaction);
    }
  });

  client.login(TOKEN);
};
bot.description = "runs the bot";
