#!/usr/bin/env node
//@ts-check

import { register } from "./register.js";
import { bot } from "./bot.js";

export const help = () => {
  `this is a Discord bot
it does things.
Discord developer portal: https://discord.com/developers/applications
In order to get your client and guild ids, open Discord and go to your settings.
On the "Advanced" page, turn on "Developer Mode".
This will enable a "Copy ID" button in the context menu when you right-click on a server icon, a user's profile, etc.
Try one of the actions below:
  - ${Object.keys(cliActions)
    .map((name) => `${name}: ${cliActions[name].description}`)
    .join("\n  - ")}
`
    .split("\n")
    .map((s) => console.log(s));
};
help.description = "this help text";

const cliActions = {
  register,
  bot,
  help,
};

export const cli = () => {
  const cliActionName = process.argv.slice(2)[0];
  if (cliActionName in cliActions) {
    const cliAction = cliActions[cliActionName];
    cliAction();
  } else {
    console.error("Error: please provide a valid command");
    help();
    process.exit(1);
  }
};
