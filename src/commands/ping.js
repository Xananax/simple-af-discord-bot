//@ts-check

export const name = "server";
export const description = "Replies with pong!";
/** @type {ReplyCommand} */
export const reply = (interaction) => interaction.reply("pong!");
