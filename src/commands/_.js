/// <reference path="../package.d.ts" />
//@ts-check

import * as ping from "./ping.js";
import * as server from "./server.js";

/** @type {Record<string, Command>} */
const commands = {
  ping,
  server,
};

export default commands;
