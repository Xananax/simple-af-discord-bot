//@ts-check

export const name = "ping";
export const description = "Some server info";

/** @type {ReplyCommand} */
export const reply = (interaction) =>
  interaction.reply(
    `Server name: ${interaction.guild.name}\nTotal members: ${interaction.guild.memberCount}`
  );
