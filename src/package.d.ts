type CommandInteraction = import("discord.js").CommandInteraction;

type ReplyCommand = (interaction: CommandInteraction) => Promise<void>;

type Command = {
  name: string;
  description: string;
  reply: ReplyCommand;
};
