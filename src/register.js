//@ts-check

import { getConfig } from "./utils/getConfig.js";
import commands from "./commands/_.js";

export const register = () => {
  const { SlashCommandBuilder } = require("@discordjs/builders");
  const { REST } = require("@discordjs/rest");
  const { Routes } = require("discord-api-types/v9");
  const { CLIENTID, GUILDID, TOKEN } = getConfig();

  const commandsAsJson = Object.values(commands).map(({ name, description }) =>
    new SlashCommandBuilder().setName(name).setDescription(description).toJSON()
  );

  const rest = new REST({ version: "9" }).setToken(TOKEN);

  rest
    .put(Routes.applicationGuildCommands(CLIENTID, GUILDID), {
      body: commandsAsJson,
    })
    .then(() => {
      console.log("Successfully registered application commands.");
    })
    .catch(console.error);
};
register.description =
  "registers the bot commands on Discord. Runs this at once before running the bot";
