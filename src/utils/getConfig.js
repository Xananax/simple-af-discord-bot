//@ts-check

import "dotenv";

export const getConfig = () => {
  const { CLIENTID = "", GUILDID = "", TOKEN = "" } = process.env;
  return {
    CLIENTID,
    GUILDID,
    TOKEN,
  };
};
