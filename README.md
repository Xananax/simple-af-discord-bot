# SIMPLE AF DISCORD BOT

Runs slash commands stored in [`src/commands`](src/commands/)

## How To:

1. Run `npm install`
2. Copy [`.env.example`](.env.example) as a new file `.env`. Fill the values
3. Add a command file following the pattern found in that directory. Check [`ping.js`](src/commands/ping.js) for an example. Check [discord.js](https://discord.js.org/) for documentation.
4. Add your new command to the index: [commands/_.js](src/commands/_.js)
5. Run `npm run register` to register the slash command on Discord
6. Run `npm run bot` to start the bot

## How to Edit `.env`:

1. Create your application at `https://discord.com/developers/applications`
2. Create a bot by going to the `bot` tab
3. Get your client id (also called "application id")
   ![](images/client_id.png)
4. Get your token
   ![](images/token.png)
5. Get your guild ID and a channel ID you want to use for the output, either by using the url (https://discord.com/channels/<GUILD>/<CHANNEL>) or right-clicking and copying the id with "developer mode" on.
6. Go to the url tab, set your permissions right, get the url
   ![](images/url_permissions.png)
7. Invite the bot to your server

## How to deploy:

1. Get a VPS
2. ssh into it, fill the `.env` file, run `npm install`
3. Run `npm install pm2@latest -g`
4. Run `pm2 startup`, and run the generated command
5. Run `pm2 start index.js --name "SAFD" --no-daemon -- bot`
6. Run `pm2 save`
7. **DONE**

**Details**:

- Run `pm2 start index.js --name "SAFD" --no-daemon -- bot` if you want the PM2 output (`--no-daemon` does that trick)
- After updating node, run `pm2 unstartup && pm2 startup`. Documentation [here](https://pm2.keymetrics.io/docs/usage/startup/)
